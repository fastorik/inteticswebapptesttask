import re


def get_file_numbers(numbers_type, file_name):
    with open(file_name, 'r') as f:
        file_content = f.read()
        numbers = re.findall(r'\d+', file_content)  # TODO:modify regex/add as func param
        f.close()

    numbers = list(map(numbers_type, numbers))
    return numbers
