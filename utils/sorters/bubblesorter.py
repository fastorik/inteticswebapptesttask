from .basesorter import BaseSorter
from utils.decorators import executed_in


class BubbleSorter(BaseSorter):
    def __init__(self, list_of_numbers):
        super().__init__(list_of_numbers)

    @executed_in
    def sort_list(self):
        has_swapped = True

        while has_swapped:
            has_swapped = False
            for i in range(len(self._list_of_numbers) - 1):
                if self._list_of_numbers[i] > self._list_of_numbers[i + 1]:

                    self._list_of_numbers[i], self._list_of_numbers[i + 1] = \
                        self._list_of_numbers[i + 1], self._list_of_numbers[i]

                    has_swapped = True
