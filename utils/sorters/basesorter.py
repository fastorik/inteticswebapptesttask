from abc import (ABC,
                 abstractmethod)


class BaseSorter(ABC):
    def __init__(self, list_of_numbers):
        self._list_of_numbers = list_of_numbers.copy()
    
    @property
    def list_of_numbers(self):
        return self._list_of_numbers.copy()
    
    @list_of_numbers.setter
    def list_of_numbers(self, list_of_numbers):
        self._list_of_numbers = list_of_numbers.copy()

    @abstractmethod
    def sort_list(self):
        pass