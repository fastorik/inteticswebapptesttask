from .basesorter import BaseSorter
from utils.decorators import executed_in


class MergeSorter(BaseSorter):
    def __init__(self, list_of_numbers):
        super().__init__(list_of_numbers)

    @executed_in
    def sort_list(self):
        list_len = len(self._list_of_numbers)
        MergeSorter.merge_sort(self._list_of_numbers, 0, list_len - 1)

    @staticmethod
    def merge(input_list, p, q, r):
        n1 = q - p + 1
        n2 = r - q
        right, left = [], []

        for i in range(n1):
            left.append(input_list[p + i])
        for j in range(n2):
            right.append(input_list[q + j + 1])

        left.append(float('inf'))
        right.append(float('inf'))

        i = j = 0
        for k in range(p, r + 1):
            if left[i] <= right[j]:
                input_list[k] = left[i]
                i += 1
            else:
                input_list[k] = right[j]
                j += 1

    @staticmethod
    def merge_sort(input_list, p, r):  # Forked optimized version
        if p < r:
            q = (p + r) // 2
            MergeSorter.merge_sort(input_list, p, q)
            MergeSorter.merge_sort(input_list, q + 1, r)
            MergeSorter.merge(input_list, p, q, r)


