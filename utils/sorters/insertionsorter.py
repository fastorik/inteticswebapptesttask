from .basesorter import BaseSorter
from utils.decorators import executed_in


class InsertionSorter(BaseSorter):
    def __init__(self, list_of_numbers):
        super().__init__(list_of_numbers)

    @executed_in
    def sort_list(self):
        for index in range(1, len(self._list_of_numbers)):
            current_value = self._list_of_numbers[index]
            current_position = index

            while current_position > 0 and self._list_of_numbers[current_position - 1] > current_value:
                self._list_of_numbers[current_position] = self._list_of_numbers[current_position - 1]
                current_position = current_position - 1

            self._list_of_numbers[current_position] = current_value


