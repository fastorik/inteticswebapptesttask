from .basesorter import BaseSorter
from utils.decorators import executed_in


class QuickSorter(BaseSorter):
    def __init__(self, list_of_numbers):
        super().__init__(list_of_numbers)

    @executed_in
    def sort_list(self):
        list_len = len(self._list_of_numbers)
        QuickSorter.quick_sort(self._list_of_numbers, 0, list_len - 1)

    @staticmethod
    def partition(input_list, start, end):
        pivot = input_list[start]
        low = start + 1
        high = end

        while True:
            while low <= high and input_list[high] >= pivot:
                high = high - 1

            while low <= high and input_list[low] <= pivot:
                low = low + 1

            if low <= high:
                input_list[low], input_list[high] = input_list[high], input_list[low]
            else:
                break

        input_list[start], input_list[high] = input_list[high], input_list[start]

        return high

    @staticmethod
    def quick_sort(input_list, start, end):
        if start >= end:
            return

        p = QuickSorter.partition(input_list, start, end)
        QuickSorter.quick_sort(input_list, start, p - 1)
        QuickSorter.quick_sort(input_list, p + 1, end)
