from functools import wraps
from time import time, sleep


def executed_in(method):
    @wraps(method)
    def wrapper(*args, **kwargs):
        start_time = time()*1000
        finished = method(*args, *kwargs)
        end_time = time()*1000

        execution_time = str(end_time - start_time)
        return finished, execution_time

    return wrapper