from django.core.exceptions import ValidationError


def validate_file(file):
    max_size_kb, file_size = 10, file.size

    if file_size > max_size_kb * 1024:
        raise ValidationError("File size limit is {} KB".format(max_size_kb))
