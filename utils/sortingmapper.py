from .sorters import (bubblesorter,
                      insertionsorter,
                      mergesorter,
                      quicksorter)


class SortTypeGetter:
    BUBBLE = 'BUBBLE'
    INSERT = 'INSERT'
    MERGE = 'MERGE'
    QUICK = 'QUICK'

    sort_map = {BUBBLE: bubblesorter.BubbleSorter,
                INSERT: insertionsorter.InsertionSorter,
                MERGE: mergesorter.MergeSorter,
                QUICK: quicksorter.QuickSorter,
                }

    @staticmethod
    def get_class_to_sort(sort_name):
        return SortTypeGetter.sort_map[sort_name]
