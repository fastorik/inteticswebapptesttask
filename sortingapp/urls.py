from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^sorting-result/(?P<pk>[0-9]+)/$', views.SuccessfulSortView.as_view(), name='result'),
]