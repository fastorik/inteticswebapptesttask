from .models import SortingResult
from django.views.generic import (CreateView,
                                  DetailView)
from utils.parsers.filesparser import get_file_numbers
from utils.sortingmapper import SortTypeGetter


class IndexView(CreateView):
    template_name = 'index.html'
    model = SortingResult
    fields = ['sorting_type', 'source_file', ]
    object_pk = None

    def form_valid(self, form):
        created_object = form.save()
        self.object_pk = created_object.pk

        numbers = get_file_numbers(int, created_object.source_file.path)
        created_object.source_list = numbers.copy()

        sorter = SortTypeGetter.get_class_to_sort(created_object.sorting_type)(numbers)
        created_object.execution_time = sorter.sort_list()[1]
        created_object.sorted_list = sorter.list_of_numbers

        created_object.list_length = len(created_object.sorted_list)
        created_object.save()

        return super().form_valid(form)

    def get_success_url(self):
        return 'sorting-result/{}/'.format(self.object_pk)


class SuccessfulSortView(DetailView):
    model = SortingResult
    template_name = 'results.html'
    context_object_name = 'sorting_record'

