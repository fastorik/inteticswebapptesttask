from django.db import models
from django.contrib.postgres.fields import ArrayField
from utils.validators import validate_file
from django.utils.translation import gettext as _


class SortingResult(models.Model):

    class SortingType(models.TextChoices):
        BUBBLE = 'BUBBLE', _('BUBBLE')
        INSERT = 'INSERT', _('INSERTION')
        MERGE = 'MERGE', _('MERGE')
        QUICK = 'QUICK', _('QUICK')

    sorting_type = models.CharField(max_length=128,
                                    choices=SortingType.choices,
                                    default=SortingType.BUBBLE,
                                    )

    source_file = models.FileField(upload_to='files/',
                                   validators=[validate_file])

    execution_time = models.FloatField(null=True, blank=True)
    source_list = ArrayField(models.IntegerField(), null=True, blank=True)
    sorted_list = ArrayField(models.IntegerField(), null=True, blank=True)
    list_length = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return str(self.id)
