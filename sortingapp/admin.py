from django.contrib import admin
from .models import SortingResult


@admin.register(SortingResult)
class SortingResultAdmin(admin.ModelAdmin):
    empty_value_display = 'Unknown'
    list_display = ['sorting_type', 'execution_time', 'source_file', 'list_length']
    search_fields = [field.name for field in SortingResult._meta.get_fields()]
    list_filter = ('sorting_type',)