from django.apps import AppConfig


class SortingappConfig(AppConfig):
    name = 'sortingapp'
