App is designed for sorting numbers with Django. Numbers have to be uploaded via file.  


How to use:  

1) Select needed sort type  

2) Upload txt file (max size - 10kb). It's better to place each number in single line  

3) Press Submit button  


TODO:  

1) More attractive design  

2) Improve regex for parsing numbers from file  
